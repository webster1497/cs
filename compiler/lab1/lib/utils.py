class TC:
	@staticmethod
	def get(file):
		tc = {}
		i = 0
		for line in file:
			tc[line.replace("\n", "")] = i
			i += 1
		return tc

class Separators:
	@staticmethod
	def get():
		sep = ["!", "*", "/", "%", "+", "-", "<", ">", "&", "|", "=", "(", ")", "[", "]", "{", "}", " ", "\t", ","]
		return sep

class Alphabet:
	def __init__(self):
		self.a = []
		self.A = []
		self.no = []
		for i in range(0, 26):
			self.a.append(chr(ord("a")+i))
			self.A.append(chr(ord("A")+i))
		for i in range(0, 10):
			self.no.append(chr(ord("0")+i))

	def getAll(self):
		return self.a + self.A + ["_"] + self.no

	def getNo(self):
		return self.no