import utils
from validation import Validation, AnalizeError

class LexicalAnalizer:
	def __init__(self):
		self.pif = {}
		self.constants = []
		self.identifiers = []
		self.tc = utils.TC.get(open("../ct"))
		self.separators = utils.Separators.get()
		self.allAlphabet = utils.Alphabet().getAll()
		self.errors = []
		self.piflist = []
		self.stCounter = 0

	def analize(self, input):
		""" analises input and constructs the pif and st tables
			input is a string
			returns [pif, identifiers, constants, errors]
		"""
		input += " " # algorithm needs an extra character at the end
		lineNo = 1
		j = 0
		i = 0
		while i < len(input):
			j = i
			if input[i] == "\n": # new lines
				lineNo += 1
				i += 1
				continue

			if input[i].isspace() or input[i] == "\t": # tabs spaces or new lines
				i += 1
				continue

			if (i < len(input) -1) and (input[i:i+2] in self.tc.keys()): # two chars operator
				self.pif[self.tc[input[i:i+2]]] = -1
				self.piflist.append([self.tc[input[i:i+2]], -1])
				i += 2
				continue # done with the segment
			elif input[i] in self.tc.keys(): # one char operator
				self.pif[self.tc[input[i:i+1]]] = -1
				self.piflist.append([self.tc[input[i:i+1]], -1])
				i += 1
				continue # done with the segment
			else: # we don't have operators or separators, we continue with the next char
				string_const = False
				if input[i] == '"': # we have a string constant
					string_const = True
					for j in range(i+1, len(input)):
						if input[j] != '"':
							continue
						else:
							break
				else:
					for j in range(i+1, len(input)): # check for separators or operators
						if input[j] == " " or input[j] == "\n" or input[j] == "\t": # tabs spaces or new lines
							break
						elif input[j] in self.tc.keys(): # one char operator
							break
						#else: # we don't have operators or separators
						#	continue

				if string_const == True: # inclue closing '""
					j += 1

				try:
					if input[i:j] in self.tc.keys(): # is reserved word
						self.pif[self.tc[input[i:j]]] = -1
						self.piflist.append([self.tc[input[i:j]], -1])
						i = j
						continue # done with the segment
					elif Validation().isIdentifier(lineNo, input[i:j]): # is identifier
						if input[i:j] not in [el[1] for el in self.identifiers]: # check for duplicates
							self.identifiers.append([len(self.identifiers) - 1, input[i:j]])
						self.pif[self.tc["identifier"]] = len(self.identifiers) - 1
						self.piflist.append([self.tc["identifier"], len(self.identifiers) - 1])
						i = j
						continue # done with the segment
				except 	AnalizeError as why: # catch syntactical errors
					self.errors.append(why)
					i = j
					continue # done with the segment
				try:
					if Validation().isConstant(lineNo, input[i:j]): # is a constant
						if input[i:j] not in [el[1] for el in self.constants]: # check for duplicates
							self.constants.append([len(self.constants) - 1, input[i:j]])
						self.pif[self.tc["constant"]] = len(self.constants) - 1
						self.piflist.append([self.tc["constant"], len(self.constants) - 1])
						i = j
						continue # done with the segment
				except AnalizeError as why: # catch syntactical errors
					self.errors.append(why)
					i = j
					continue # done with the segment
		print "pif\n" + str(self.piflist)
		print "constants\n" + str(self.constants)
		print "identifiers\n" + str(self.identifiers)
		for er in self.errors:
			print "errors\n" + str(er)

		print "\n\n\n"

		return [self.pif, self.identifiers, self.constants, self.errors]