from utils import Alphabet, Separators

class AnalizeError(Exception):
	def __init__(self, msg):
		self.msg = msg

	def __str__(self):
		return self.msg

class Validation:
	def validateString(self,lineNo, input):
		for c in input[1:len(input) - 1]:
			if c not in Alphabet().getAll() + Separators().get():
				raise AnalizeError("Non alphabet character at line " + str(lineNo) + " in string'" + input + "'")

	def validateIdentifier(self,lineNo, input):
		if len(input) > 250:
			raise AnalizeError("Identifier exceedes 250 character lengh at line " + str(lineNo))
		for c in input:
			if c not in Alphabet().getAll():
				raise AnalizeError("Non alphabet character at line " + str(lineNo) + " in identifier'" + input + "'")

	def validateInteger(self, lineNo, input):
		for c in input:
			if c not in Alphabet().getNo():
				raise AnalizeError("Non numerical character at line " + str(lineNo) + " in integer'" + input + "'")
		if input[0] == "0":
			raise AnalizeError("Integer '" + str(input) + "' starts with 0 at line " + str(lineNo))

	def isConstant(self,lineNo, input):
		if input[0] == '"' and input[len(input) - 1] == '"':
			self.validateString(lineNo, input)
			return "string"
		else:
			self.validateInteger(lineNo, input)
			return "integer"

	def isIdentifier(self, lineNo, input):
		if input[0] in Alphabet().getNo():
			return False
		if input[0] == '"':
			return False

		self.validateIdentifier(lineNo, input)
		return True

