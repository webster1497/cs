import unittest
import StringIO

from utils import TC

class TestTc(unittest.TestCase):
	def testGet(self):
		input = """identifier
constant
main
while
if
else
read
write
;
!
*
/
%
+
-
<
>
<=
>=
==
!=
&&
||
=
(
)
[
]
{
}"""
		tc = TC().get(StringIO.StringIO(input))
		input = input.split()
		for i in range(len(tc)):
			self.assertEqual(tc[input[i]], i)


def main():
	unittest.main()

if __name__ == '__main__':
	main()