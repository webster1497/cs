import unittest
from validation import Validation, AnalizeError

class TestValidation(unittest.TestCase):
	def testValidateString(self):
		string = '"4abc_09 23"'
		self.assertEqual(Validation().validateString(3, string), None)
		string = '"4a%bc0#923"'
		self.assertRaises(AnalizeError, Validation().validateString, 5, string)

	def testValidateIdentifier(self):
		string = '4abc0923'
		self.assertEqual(Validation().validateIdentifier(3, string), None)
		string = '"4abc09#23"'
		self.assertRaises(AnalizeError, Validation().validateIdentifier, 5, string)
		string = 'a'
		for i in range(255):
			string += "a"
		self.assertRaises(AnalizeError, Validation().validateIdentifier, 5, string)

	def testValidateInteger(self):
		string = '123'
		self.assertEqual(Validation().validateInteger(3, string), None)
		string = "12*&3as"
		self.assertRaises(AnalizeError, Validation().validateInteger, 2, string)
		string = "00145"
		self.assertRaises(AnalizeError, Validation().validateInteger, 2, string)

	def testIsConstant(self):
		string = '"afgs_"'
		self.assertEqual(Validation().isConstant(3, string), "string")
		string = '1264'
		self.assertEqual(Validation().isConstant(3, string), "integer")

def main():
	unittest.main()

if __name__ == '__main__':
	main()