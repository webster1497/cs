import unittest
from lexical_analizer import LexicalAnalizer
from validation import AnalizeError

class TestLexicalAnalizer(unittest.TestCase):
	def testAnalize(self):
		# good input
		input = """int main(){
a = 54;
1b = 90
str="sum of a and b is"
write a+b;
if(a >b)
	write "a is bigger than b"
else
	write "something"
while(i <= 100)
	a = b /i;
"""
		lex = LexicalAnalizer()
		#self.assertEqual(lex.analize(input), [{0: 3, 1: 5, 2: -1, 3: -1, 5: -1, 6: -1, 7: -1, 9: -1, 10: -1, 14: -1, 16: -1, 19: -1, 20: -1, 26: -1, 27: -1, 28: -1, 31: -1},
		#									  {0: 'a', 1: 'b', 2: 'str', 3: 'i'}, {0: '54', 1: '90', 2: '"sum of a and b is"', 3: '"a is bigger than b"', 4: '"something"', 5: '100'},
		#									[]])
		lex.analize(input)
		# invalid identifier
		input = "^acv= 98"
		try:
			lex.analize(input)
		except AnalizeError as why:
			self.assertTrue("identifier" in str(why))

		# invalid integer
		input = "acf= 098"
		try:
			lex.analize(input)
		except AnalizeError as why:
			self.assertTrue("starts with 0" in str(why))

		input = "acf= 9_sd8"
		try:
			lex.analize(input)
		except AnalizeError as why:
			self.assertTrue("in integer" in str(why))

def main():
	unittest.main()

if __name__ == '__main__':
	main()