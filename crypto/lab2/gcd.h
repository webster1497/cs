#include <gmpxx.h>

class Gcd
{
public:
	/* euclid algoritm */
	static mpz_class gcd1(mpz_class x, mpz_class y)
	{
		mpz_class aux;
		x = abs(x);
		y = abs(y);

		while(x != 0){
			aux = x;
			x = y % x;
			y = aux;
		}
		return y;
	}

	/* recursive euclid algorithm */
	static mpz_class gcd2(mpz_class x, mpz_class y)
	{
		if(y == 0)
			return x;
		else
			return gcd2(y, x % y);
	}

	/* substraction algotithm */
	static mpz_class gcd3(mpz_class x, mpz_class y)
	{
		mpz_class aux;
		x = abs(x);
		y = abs(y);

		while(x != y){
			while(x > y){
				aux = x - y;
				x = aux;
			}
			while(y > x){
				aux = y - x;
				y = aux;
			}
		}
		if(x > 1)
			return x;
		return y;
	}


};