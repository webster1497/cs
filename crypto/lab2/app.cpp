#include <iostream>
#include <gmpxx.h>
#include "gcd.h"
#include <time.h>
#include <fstream>
#include <iomanip>

using namespace std;
int main()
{
	mpz_class a,b,c;
	clock_t t, delta_t;

	cout.setf(ios::fixed, ios::floatfield);
	cout.setf(ios::showpoint);

	ifstream file;
	file.open("gcd.data");
	string x, y;

	int i = 1;
	while(getline (file,x)){
		getline (file,y);
		a = x;
		b = y;

		cout<< "Test "<< i <<" a= " << a << ", b= " << b <<endl;

		t = clock();
		c = Gcd::gcd1(a, b);
		delta_t = clock() - t;
		cout << "euclid:           " << c << " in " << ((float)delta_t) / CLOCKS_PER_SEC << " sec" <<endl;

		t = clock();
		c = Gcd::gcd2(a, b);
		delta_t = clock() - t;
		cout << "recursive euclid: " << c << " in " << ((float)delta_t) / CLOCKS_PER_SEC << " sec" <<endl;


		t = clock();
		c = Gcd::gcd3(a, b);
		delta_t = clock() - t;
		cout << "substraction:     " << c << " in " << ((float)delta_t) / CLOCKS_PER_SEC << " sec" <<endl;

		cout<<endl;
		i++;
	}

	file.close();
	return 0;
}