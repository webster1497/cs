class ValidationException(Exception):
	def __init__(self, value):
		self.value = value
	def message(self):
		return str(self.value)

from fractions import gcd

class Validator:
	""" performs validation of input acording to the Affine cipher """
	def validateKey(self, a, n):
		""" verifies if the two elements of the key are coprime, key must ne 2 natural numners """
		if gcd(a,n) != 1:
			raise ValidationException("The key and the lenght of the alphabet are not coprime!")

	def validatePlainText(self, text, alphabet):
		""" verify if text contains only characters from alphabet """
		for char_t in text:
			char_is = False
			for char_a in alphabet:
				if char_a.lower() == char_t.lower():
					char_is = True
			if not char_is:
				raise ValidationException("The text contains characters out of the alphabet!")