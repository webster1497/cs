import math

class AffineCipher:
	""" class that implements the Affine cipher """
	def order(self, char, alphabet):
		""" return the order of the char in alphabet """
		for i, c in enumerate(alphabet.lower()):
			if c == char.lower():
				return i
		return None
	def inverse(self, key, alphabet):
		""" compute the imverse of key in mod len of alphabet """
		for i in range(1, len(alphabet)):
			if key[0] * i % 27 == 1:
				return i;
		return None

	def encrypt(self, text, alphabet, key):
		""" encrypt the given text using the alphabet and key """
		encrypted = ""
		for char in text:
			i = (self.order(char, alphabet) * key[0] + key[1]) % len(alphabet)
			encrypted += alphabet[i]
		return encrypted

	def decrypt(self, text, alphabet, key):
		""" decrypt the given text using the alphabet and key """
		decrypted = ""
		for char in text:
			i = (self.inverse(key, alphabet) * (self.order(char, alphabet) - key[1]) % len(alphabet))
			decrypted += alphabet[i]
		return decrypted
