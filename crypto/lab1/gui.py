import sys
from PyQt4 import QtGui, QtCore
from validator import *
from affine import AffineCipher

class Gui(QtGui.QWidget):
	""" Primary class for UI """
	def __init__(self):
		super(Gui, self).__init__()
		self.initUI()

	def initUI(self):
		""" initialise the window """
		self.alphabet = []
		self.alphabetGrid = []
		for i in range(0, 26):
			self.alphabet.append(chr(ord("A")+i))
		self.alphabet.append("_") # the blank character

		row = 0
		grid = QtGui.QGridLayout()
		alphabet_lb = QtGui.QLabel("The alphabet")
		grid.addWidget(alphabet_lb, row, 0, 1, 8)
		row += 1

		# calculate alphabet position
		pos = []
		for i in range(1,4):
			for j in range(int(len(self.alphabet)/3)):
				pos.append([i, j])

		# add alphabet to grid
		for i, character in enumerate(self.alphabet):
			cb = QtGui.QCheckBox(character, self)
			cb.toggle()
			self.alphabetGrid.append(cb)
			grid.addWidget(cb, pos[i][0], pos[i][1])
		row += 3

		# add key elements
		keys_lb = QtGui.QLabel("The key")
		grid.addWidget(keys_lb, row, 0, 1, 3)

		self.key_a = QtGui.QSpinBox()
		self.key_a.setMinimum (-100)
		grid.addWidget(self.key_a, row, 3, 1, 2)
		self.key_b = QtGui.QSpinBox()
		self.key_b.setMinimum (-100)
		grid.addWidget(self.key_b, row, 7, 1, 2)
		row += 1

		# encrypt/decrypt buttons
		encrypt_btn = QtGui.QPushButton("Encrypt")
		grid.addWidget(encrypt_btn, row, 0, 1, 4)
		decrypt_btn = QtGui.QPushButton("Decrypt")
		grid.addWidget(decrypt_btn, row, 5, 1, 4)
		row += 1

		# input/output button elements
		input_lb = QtGui.QLabel("Input")
		grid.addWidget(input_lb, row, 0, 1, 2)
		row += 1
		self.input_txt = QtGui.QTextEdit()
		grid.addWidget(self.input_txt, row, 0, 1, 9)
		row += 1
		output_lb = QtGui.QLabel("Output")
		grid.addWidget(output_lb, row, 0, 1, 2)
		row += 1
		self.output_txt = QtGui.QTextEdit()
		grid.addWidget(self.output_txt, row, 0, 1, 9)

		# add button slots
		encrypt_btn.clicked.connect(self.encryptAction)
		decrypt_btn.clicked.connect(self.decryptAction)

		self.setLayout(grid)
		self.setWindowTitle('Affine cipher')
		self.show()

	def keyPressEvent(self, e):
		""" reimplement to close on esc pressed """
		if e.key() == QtCore.Qt.Key_Escape:
			self.close()

	def getAlphabet(self):
		""" returns the chosen alphabet from the gui as a string """
		alphabet = ""
		for cb in  self.alphabetGrid:
			if cb.isChecked():
				alphabet += cb.text()
		return str(alphabet)

	def encryptAction(self, event):
		""" encrypt input text when encrypt button pressed """
		validator = Validator()
		try:
			validator.validateKey(int(self.key_a.value()), len(self.getAlphabet()))
			validator.validatePlainText(str(self.input_txt.toPlainText()), self.getAlphabet())
		except ValidationException as error:
			QtGui.QMessageBox.warning(self, 'Warning', error.message(), QtGui.QMessageBox.Ok)
			return
		# try:
		# 	validator.validatePlainText(str(self.input_txt.toPlainText()), self.getAlphabet())
		# except ValidationException as error:
		# 	QtGui.QMessageBox.warning(self, 'Warning', error.message(), QtGui.QMessageBox.Ok)
		# 	return
		cipher = AffineCipher()
		encrypted = cipher.encrypt(str(self.input_txt.toPlainText()), str(self.getAlphabet()), [int(self.key_a.value()), int(self.key_b.value())])
		self.output_txt.setPlainText(encrypted)

	def decryptAction(self, event):
		""" decrypt input text when decrypt button pressed """
		validator = Validator()
		try:
			validator.validateKey(int(self.key_a.value()), len(self.getAlphabet()))
			validator.validatePlainText(str(self.output_txt.toPlainText()), self.getAlphabet())
		except ValidationException as error:
			QtGui.QMessageBox.warning(self, 'Warning', error.message(), QtGui.QMessageBox.Ok)
			return
		# try:
		# 	validator.validatePlainText(str(self.output_txt.toPlainText()), self.getAlphabet())
		# except ValidationException as error:
		# 	QtGui.QMessageBox.warning(self, 'Warning', error.message(), QtGui.QMessageBox.Ok)
		# 	return
		cipher = AffineCipher()
		decrypted = cipher.decrypt(str(self.output_txt.toPlainText()), str(self.getAlphabet()), [int(self.key_a.value()), int(self.key_b.value())])
		self.input_txt.setPlainText(decrypted.lower())


def main():

	app = QtGui.QApplication(sys.argv)
	ex = Gui()
	sys.exit(app.exec_())


if __name__ == '__main__':
	main()
